from pathlib import Path

from diagrams import Cluster, Diagram
from diagrams.onprem.network import HAProxy
from diagrams.aws.network import ELB

HEALTHY_GRAPH_ATTR = {"bgcolor": "palegreen"}
UNHEALTHY_GRAPH_ATTR = {"bgcolor": "salmon"}
DEPOOLED_GRAPH_ATTR = {"bgcolor": "palevioletred"}

with Diagram("text@eqiad", direction="TB", filename="all_healthy", show=False):
    servers = []
    with Cluster("pooled=yes"):
        for host_id in range(1075, 1091, 2):
            with Cluster("status=healthy", graph_attr=HEALTHY_GRAPH_ATTR):
                servers.append(HAProxy(f"cp{host_id}"))

    ELB("LB") >> servers

with Diagram("text@eqiad", direction="TB", filename="one_unhealthy", show=False):
    servers = []
    with Cluster("pooled=yes"):
        for host_id in range(1075, 1089, 2):
            with Cluster("status=healthy", graph_attr=HEALTHY_GRAPH_ATTR):
                servers.append(HAProxy(f"cp{host_id}"))
        with Cluster("status=unhealthy", graph_attr=UNHEALTHY_GRAPH_ATTR):
            servers.append(HAProxy("cp1089"))

    ELB("LB") >> servers

with Diagram("text@eqiad", direction="TB", filename="human_error", show=False):
    servers = []
    with Cluster("pooled=yes"):
        for host_id in range(1075, 1079, 2):
            with Cluster("status=healthy", graph_attr=HEALTHY_GRAPH_ATTR):
                servers.append(HAProxy(f"cp{host_id}"))
    with Cluster("pooled=no", graph_attr=DEPOOLED_GRAPH_ATTR):
        for host_id in range(1079, 1091, 2):
            with Cluster("status=healthy", graph_attr=HEALTHY_GRAPH_ATTR):
                servers.append(HAProxy(f"cp{host_id}"))

    ELB("LB") >> servers
